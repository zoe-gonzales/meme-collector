# meme-collector

## Summary
This is a simple app using express and node to populate and add memes. Materialize, Google Fonts, and Subtle Patterns were used for styling.

![meme screenshot](screenshot.png)

Deployed at https://thawing-plains-13473.herokuapp.com/