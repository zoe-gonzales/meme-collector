
var express = require('express');
var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

var port = process.env.PORT || 8080;

// html requests
app.use(require('./routes/htmlRoutes'));

// post requests
app.use(require('./routes/apiRoutes'));

// listening on port 8080
app.listen(port, function(){
    console.log(`Listening on port ${port}`);
});
