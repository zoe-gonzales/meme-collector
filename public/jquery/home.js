$(document).ready(function(){
    $('#view-memes').click(function(e){
        $.ajax({
            method: 'GET',
            url: '/currentmemes/'
        }).then(function(data){
            if (data){
                for (var i=0; i < data.length; i++){
                    var card = $('<div>');
                    card.addClass('card');
                    var cardImg = $('<div>');
                    cardImg.addClass('card-image');
                    var memeImg = $('<img>');
                    cardImg.append(memeImg);
                    memeImg.attr('src', data[i].source);
                    var cardCnt = $('<div>');
                    cardCnt.addClass('card-content');
                    var title = $('<p>');
                    title.text(`Title: ${data[i].title}`);
                    var category = $('<p>');
                    category.text(`Category: ${data[i].category}`);
                    cardCnt.append(title, category);
                    card.append(cardImg, cardCnt);
                    $('#meme-list').append(card);
                }
            }
        })
    });
});