$(document).ready(function(){
    $('#add-meme').click(function(e){
        e.preventDefault();

        var title = $('#title').val().trim();
        var category = $('#category').val().trim();
        var source = $('#url').val().trim();

        if (!title || !category || !source){
            $('#message').text('All fields required.'); 
        } else {
            var meme = {
                title: title,
                category: category,
                source: source
            };

            $.ajax({
                method: 'POST',
                url: '/api/memes',
                data: meme
            }).then(function(data){
                if (data){
                    $('#message').text('Meme successfully added!');
                } else $('#message').text('It seems an error occurred. Please try again!');               
            });
        }
    });
});