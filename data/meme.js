

var memes = [
    {
        title: 'Grumpy Cat\'s Saturday Afternoon',
        category: 'cats',
        source: 'https://www.memerewards.com/images/2017/09/30/dce7994c155505d180238071c0d0c380--grumpy-cat-quotes-cat-memes8c26dea290ae8550.jpg'
    },
    {
        title: 'Grumpy Cat\'s Saturday Afternoon',
        category: 'cats',
        source: 'https://3milliondogs.com/blog-assets-two/2015/08/groxneqokwg6awhcezmz.jpg'
    },
    {
        title: 'Savage Patrick',
        category: 'spongebob',
        source: 'https://i.chzbgr.com/full/9134842112/h8640A606/'
    }
];

module.exports = memes;