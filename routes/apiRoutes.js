
var express = require('express');
var memes = require('../data/meme');
var router = express.Router();

router.get('/currentmemes/', function(request, response){
    return response.json(memes);
});

router.post('/api/memes', function(request, response){
    var newMeme = request.body;
    memes.push(newMeme);
    console.log(newMeme);
    response.json(memes);
});

module.exports = router;