var express = require('express');
var path = require('path');
var router = express.Router();

router.get('/', function(request, response){
    response.sendFile(path.join(__dirname + '/../public/home.html'));
});

router.get('/add', function(request, response){
    response.sendFile(path.join(__dirname + '/../public/add.html'));
});

module.exports = router;